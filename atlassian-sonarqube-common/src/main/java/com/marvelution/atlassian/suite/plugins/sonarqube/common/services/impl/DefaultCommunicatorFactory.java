/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.services.impl;

import com.atlassian.applinks.api.*;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.applinks.SonarQubeApplicationType;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.Communicator;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.CommunicatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Default implementation of {@link CommunicatorFactory}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultCommunicatorFactory implements CommunicatorFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCommunicatorFactory.class);
	private final ApplicationLinkService applicationLinkService;
	private final AuthenticationConfigurationManager authenticationConfigurationManager;

	/**
	 * Constructor
	 *
	 * @param applicationLinkService the {@link com.atlassian.applinks.api.ApplicationLinkService} implementation
	 * @param authenticationConfigurationManager
	 *                               the {@link com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager} implementation
	 */
	public DefaultCommunicatorFactory(ApplicationLinkService applicationLinkService, AuthenticationConfigurationManager
			authenticationConfigurationManager) {
		this.applicationLinkService = applicationLinkService;
		this.authenticationConfigurationManager = authenticationConfigurationManager;
	}

	@Override
	public Communicator get(ApplicationId applicationId) {
		checkNotNull(applicationId);
		try {
			return get(applicationLinkService.getApplicationLink(applicationId));
		} catch (TypeNotInstalledException e) {
			throw new IllegalArgumentException("Unable to get an ApplicationLink for " + applicationId);
		}
	}

	@Override
	public Communicator get(ApplicationLink applicationLink) {
		checkNotNull(applicationLink);
		LOGGER.debug("Getting a Communicator for ApplicationLink '" + applicationLink.getName() + "' of type " +
				applicationLink.getType().getI18nKey());
		if (applicationLink.getType() instanceof SonarQubeApplicationType) {
			return new DefaultCommunicator(authenticationConfigurationManager, applicationLink);
		} else {
			throw new IllegalArgumentException(applicationLink.getName() + " is not a SonarQube Application Link");
		}
	}

}
