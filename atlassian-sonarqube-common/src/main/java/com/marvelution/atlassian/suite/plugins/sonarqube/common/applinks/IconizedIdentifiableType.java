/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.applinks;

import com.atlassian.applinks.spi.application.IdentifiableType;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.utils.SonarQubeCommonPluginUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * New Abstract implementation of the {@link com.atlassian.applinks.spi.application.IdentifiableType} interface that implemented a
 * standard getIconUrl() method
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class IconizedIdentifiableType implements IdentifiableType {

	private static final Logger LOGGER = LoggerFactory.getLogger(IconizedIdentifiableType.class);
	private final SonarQubeCommonPluginUtils pluginUtil;
	private final WebResourceManager webResourceManager;

	/**
	 * Constructor
	 *
	 * @param pluginUtil the {@link SonarQubeCommonPluginUtils}
	 * @param webResourceManager the {@link com.atlassian.plugin.webresource.WebResourceManager} implementation
	 */
	protected IconizedIdentifiableType(SonarQubeCommonPluginUtils pluginUtil, WebResourceManager webResourceManager) {
		this.pluginUtil = pluginUtil;
		this.webResourceManager = webResourceManager;
	}

	/**
	 * Get the Icon {@link java.net.URI} for this {@link com.atlassian.applinks.spi.application.IdentifiableType}
	 *
	 * @return the icon {@link java.net.URI}
	 */
	public final URI getIconUrl() {
		try {
			return new URI(this.webResourceManager.getStaticPluginResource(pluginUtil.getPluginKey()+ ":sonarqube-images", "images",
					UrlMode.ABSOLUTE) + "/icon16_" + getId().get() + ".png");
		} catch (URISyntaxException e) {
			LOGGER.warn("Unable to find the icon for type {}", getId().get(), e);
		}
		return null;
	}

}
