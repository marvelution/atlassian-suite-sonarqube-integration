/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.servlets;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.Communicator;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.CommunicatorFactory;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.HostApplicationFacade;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.RestResponse;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

/**
 * {@link HttpServlet} implementation for providing Widget HTML code
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class WidgetServlet extends HttpServlet {

	private static final List<String> IGNORED_PARAMETERS = Lists.newArrayList("applicationId", "localKey", "remoteKey", "action");
	private final HostApplicationFacade hostApplicationFacade;
	private final CommunicatorFactory communicatorFactory;
	private final TemplateRenderer templateRenderer;
	private final ApplicationProperties applicationProperties;

	/**
	 * Default Constructor
	 *
	 * @param hostApplicationFacade the {@link HostApplicationFacade}
	 * @param communicatorFactory   the {@link CommunicatorFactory}
	 * @param templateRenderer      the {@link TemplateRenderer}
	 * @param applicationProperties the {@link ApplicationProperties}
	 */
	public WidgetServlet(HostApplicationFacade hostApplicationFacade, CommunicatorFactory communicatorFactory,
	                     TemplateRenderer templateRenderer, ApplicationProperties applicationProperties) {
		this.hostApplicationFacade = hostApplicationFacade;
		this.communicatorFactory = communicatorFactory;
		this.templateRenderer = templateRenderer;
		this.applicationProperties = applicationProperties;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			final String action = getParameter(req, "action", "view");
			final String localKey = getParameter(req, "localKey");
			ApplicationId applicationId = new ApplicationId(getParameter(req, "applicationId"));
			try {
				if ("view".equalsIgnoreCase(action)) {
					final String remoteKey = getParameter(req, "remoteKey");
					EntityLink entityLink = hostApplicationFacade.getEntityLink(localKey, applicationId, remoteKey);
					if (entityLink != null) {
						UriBuilder uriBuilder = getUri("/widget", req).queryParam("resource", entityLink.getKey());
						Document html = getHtml(entityLink.getApplicationLink(), uriBuilder);
						StringWriter writer = new StringWriter();
						HashMap<String, Object> context = Maps.newHashMap();
						context.put("baseUrl", applicationProperties.getBaseUrl());
						context.put("applicationId", entityLink.getApplicationLink().getId());
						templateRenderer.render("/templates/widget-ajax-proxy.vm", context, writer);
						html.select("head").first().append(writer.toString());
						resp.setStatus(HttpServletResponse.SC_OK);
						resp.getWriter().write(html.html());
					} else {
						resp.sendError(HttpServletResponse.SC_NOT_FOUND, "No remote entity found");
					}
				} else if ("config".equalsIgnoreCase(action)) {
					ApplicationLink applicationLink = hostApplicationFacade.getApplicationLink(applicationId);
					if (applicationLink != null) {
						Document html = getHtml(applicationLink, getUri("/widget_config", req));
						resp.setStatus(HttpServletResponse.SC_OK);
						resp.getWriter().write(html.html());
					} else {
						resp.sendError(HttpServletResponse.SC_NOT_FOUND, "No remote application found");
					}
				} else {
					resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Unsupported action: " + action);
				}
			} catch (CredentialsRequiredException e) {
				resp.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
			} catch (Exception e) {
				resp.sendError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
			}
		} catch (IllegalArgumentException e) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		}
	}

	/**
	 * Build a {@link UriBuilder} based on the incoming {@link HttpServletRequest}
	 *
	 * @param path the base path of the service on SonarQube
	 * @param req  the incoming {@link HttpServletRequest}
	 * @return the {@link UriBuilder}
	 */
	private UriBuilder getUri(String path, HttpServletRequest req) {
		UriBuilder uriBuilder = UriBuilder.fromPath(path);
		Enumeration parameterNames = req.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String parameterName = (String) parameterNames.nextElement();
			if (!IGNORED_PARAMETERS.contains(parameterName)) {
				uriBuilder = uriBuilder.queryParam(parameterName, req.getParameter(parameterName));
			}
		}
		return uriBuilder;
	}

	/**
	 * Get the {@link Document} HTML of the SonarQube service
	 *
	 * @param applicationLink the {@link ApplicationLink} of the SonarQube server to query
	 * @param uriBuilder      the {@link UriBuilder} used to build the request URL
	 * @return the {@link Document}
	 * @throws CredentialsRequiredException
	 * @throws IOException
	 */
	private Document getHtml(ApplicationLink applicationLink, UriBuilder uriBuilder) throws CredentialsRequiredException, IOException {
		Communicator communicator = communicatorFactory.get(applicationLink);
		RestResponse response = communicator.get(uriBuilder.build().toASCIIString());
		Document html = Jsoup.parse(response.getResponseBody());
		String baseUrl = applicationLink.getRpcUrl().toASCIIString();
		for (Element media : html.select("[src]")) {
			if (media.hasAttr("src")) {
				String src = media.attr("src");
				media.attr("src", baseUrl + src);
			}
		}
		for (Element link : html.select("link[href]")) {
			if (link.hasAttr("href")) {
				String href = link.attr("href");
				link.attr("href", baseUrl + href);
			}
		}
		for (Element link : html.select("a[href]")) {
			if (link.hasAttr("href")) {
				String src = link.attr("href");
				link.attr("href", baseUrl + src);
				link.attr("target", "_blank");
			}
		}
		return html;
	}

	/**
	 * Getter for a request parameter
	 *
	 * @param req          the {@link HttpServletRequest}
	 * @param key          the parameter key
	 * @param defaultValue the default value
	 * @return the parameter value
	 * @throws IllegalArgumentException in case there is no parameter value in the request and the default in {@code empty}
	 */
	private String getParameter(HttpServletRequest req, String key, String defaultValue) {
		String value = req.getParameter(key);
		if (StringUtils.isNotBlank(value)) {
			return value;
		} else if (StringUtils.isNotBlank(defaultValue)) {
			return defaultValue;
		} else {
			throw new IllegalArgumentException("Missing required parameter: " + key);
		}
	}

	/**
	 * Getter for a request parameter
	 *
	 * @param req the {@link HttpServletRequest}
	 * @param key the parameter key
	 * @return the parameter value
	 * @throws IllegalArgumentException in case there is no parameter value in the request
	 * @see #getParameter(javax.servlet.http.HttpServletRequest, String, String)
	 */
	private String getParameter(HttpServletRequest req, String key) {
		return getParameter(req, key, null);
	}

}
