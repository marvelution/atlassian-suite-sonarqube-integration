/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.services.impl;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType;
import com.atlassian.applinks.api.application.confluence.ConfluenceSpaceEntityType;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReference;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReferences;

import javax.annotation.Nullable;

/**
 * Confluence Specific implementation for {@link AbstractHostApplicationFacade}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ConfluenceApplicationFacade extends AbstractHostApplicationFacade {

	private final SpaceManager spaceManager;
	private final PermissionManager permissionManager;
	private final Function<Space, EntityReference> spaceToEntityReference = new Function<Space, EntityReference>() {
		@Override
		public EntityReference apply(@Nullable Space space) {
			if (space != null) {
				return new EntityReference(space.getKey(), space.getName());
			}
			return null;
		}
	};

	/**
	 * Constructor
	 *
	 * @param applicationLinkService the {@link com.atlassian.applinks.api.ApplicationLinkService}
	 * @param typeAccessor      the Applinks {@link com.atlassian.applinks.spi.util.TypeAccessor}
	 * @param entityLinkService the {@link com.atlassian.applinks.api.EntityLinkService}
	 * @param spaceManager      the {@link com.atlassian.confluence.spaces.SpaceManager}
	 * @param permissionManager the {@link PermissionManager}
	 */
	public ConfluenceApplicationFacade(ApplicationLinkService applicationLinkService, TypeAccessor typeAccessor,
	                                   EntityLinkService entityLinkService, SpaceManager spaceManager,
	                                   PermissionManager permissionManager) {
		super(applicationLinkService, entityLinkService, typeAccessor, ConfluenceApplicationType.class, ConfluenceSpaceEntityType.class);
		this.spaceManager = spaceManager;
		this.permissionManager = permissionManager;
	}

	@Override
	protected Object getRawEntity(String key) {
		return spaceManager.getSpace(key);
	}

	@Override
	public EntityReference getEntity(String key) {
		Space space = spaceManager.getSpace(key);
		if (permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, space)) {
			return spaceToEntityReference.apply(space);
		} else {
			return null;
		}
	}

	@Override
	public EntityReferences getEntities() {
		SpacesQuery query = SpacesQuery.newQuery().forUser(AuthenticatedUserThreadLocal.getUser()).withPermission(Permission.VIEW.toString())
				.build();
		return new EntityReferences(Iterables.transform(spaceManager.getAllSpaces(query), spaceToEntityReference));
	}

	@Override
	public boolean isAuthenticated() {
		return AuthenticatedUserThreadLocal.getUser() != null;
	}

	@Override
	public boolean isSystemAdministrator() {
		return permissionManager.isConfluenceAdministrator(AuthenticatedUserThreadLocal.getUser());
	}

}
