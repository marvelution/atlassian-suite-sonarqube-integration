/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

if (AJS.namespace === undefined) {
	AJS.namespace = function (namespace, context, value) {
		var names = namespace.split(".");
		context = context || window;
		for (var i = 0, n = names.length - 1; i < n; i++) {
			var x = context[names[i]];
			context = (x != null) ? x : context[names[i]] = {}
		}
		return context[names[i]] = value || {}
	}
}

AJS.namespace("SonarQubeCommon");

SonarQubeCommon.serializeProperties = function (deserialized) {
	var serialized = {};
	AJS.$.each(deserialized, function () {
		serialized[this.key] = this.value;
	});
	return encodeURIComponent(AJS.$.param(serialized));
};

SonarQubeCommon.deserializeProperties = function (serialized) {
	var deserialized = [];
	var asParam = AJS.$.deparam(decodeURIComponent(serialized));
	AJS.$.each(asParam, function (key) {
		deserialized.push({
			key: key,
			value: asParam[key]
		});
	});
	return deserialized;
};
