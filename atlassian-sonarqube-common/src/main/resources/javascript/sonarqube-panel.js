/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

AJS.namespace("SonarQubePanel.dashboard");

SonarQubePanel.dashboard.defaults = {
	baseUrl: AJS.contextPath || "",
	localEntity: "",
	remoteEntity: "",
	remoteApplication: "",
	dashboards: undefined
};

SonarQubePanel.dashboard.panel = function (options) {
	var settings = AJS.$.extend({}, SonarQubePanel.dashboard.defaults, options);
	AJS.$("#sonarqube-dashboard-container").data(settings);
	if (settings.dashboards && AJS.$.isArray(settings.dashboards)) {
		AJS.$("#sonarqube-panel-toolbar").removeClass("hidden");
		var dashboardSelector = AJS.$("#dropdown2-dashboards ul");
		AJS.$.each(settings.dashboards, function (index, dashboard) {
			AJS.log("Adding dashboard " + dashboard.name);
			var option = AJS.$(SonarQubeTemplate.panel.dashboardOption({
				id: dashboard.id,
				name: dashboard.name
			}));
			option.data(dashboard);
			option.click(function () {
				var element = AJS.$(this);
				AJS.log("Changing dashboard to " + element.text());
				SonarQubePanel.dashboard.render(element.data());
			});
			dashboardSelector.append(option);
		});
		SonarQubePanel.dashboard.render(settings.dashboards[0]);
	} else if (settings.dashboards && AJS.$.isPlainObject(settings.dashboards)) {
		AJS.$("#sonarqube-panel-toolbar").addClass("hidden");
		SonarQubePanel.dashboard.render(settings.dashboards);
	}
};

SonarQubePanel.dashboard.render = function (dashboard) {
	AJS.$("#selected-sonarqube-dashboard").text(dashboard.name);
	var container = AJS.$("#sonarqube-dashboard-container");
	var settings = container.data();
	container.empty().append(SonarQubeTemplate.panel.dashboard({dashboard: dashboard}));
	AJS.$.each(dashboard.columns, function (index, column) {
		var columnElement = AJS.$("#dashboard-column-" + (index + 1));
		AJS.$.each(column.widget, function (index, widget) {
			columnElement.append(SonarQubeWidget.template({
				baseUrl: settings.baseUrl,
				key: widget.key,
				entity: settings.localEntity,
				remoteEntity: settings.remoteEntity,
				remoteApplication: settings.remoteApplication,
				width: columnElement.width(),
				properties: widget.properties,
				editable: false
			}));
		});
	});
};
