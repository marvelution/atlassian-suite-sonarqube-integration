/*
 * SonarQube Integration for Confluence
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Determines whether we're running in an iframe.
if (window.top.AJS && window.top != window.self) {
	window.top.AJS.log("Running within a frame/iframe/object.");
	if (window.top.AJS.MacroBrowser) {
		window.top.AJS.log("Macro browser is present. So we're in a preview iframe from macro browser.");
		var mb = window.top.AJS.MacroBrowser;
		mb.widgetPropertiesChanged = false;
		SonarQubeWidget.saveMacroProperties = function (properties) {
			mb.widgetPropertiesChanged = true;
			mb.widgetProperties = properties;
			mb.Macros["sonarqube-widget"].manipulateMarkup();
		};
	} else {
		window.top.AJS.log("Not running within macro browser.");
	}
} else {
	AJS.log("Not running within a frame/iframe/object, presumably in normal page rendering...");
}

SonarQubeWidget.macro = function (options) {
	var container = AJS.$(options.containerId);
	var widget = SonarQubeWidget.template({
		baseUrl: AJS.contextPath(),
		key: options.widgetKey,
		entity: options.spacekey,
		remoteEntity: options.resourceKey,
		remoteApplication: options.applicationId,
		properties: SonarQubeCommon.deserializeProperties(options.propertiesAsString),
		propertiesChanged: SonarQubeWidget.saveMacroProperties,
		editable: options.editable,
		width: container.width()
	});
	container.append(widget);
};
