/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.filter;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.xml.XmlPage;
import com.marvelution.atlassian.suite.plugins.sonarqube.BaseITCase;
import com.marvelution.atlassian.suite.plugins.sonarqube.utils.ApplicationIdUtilsTest;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import static junit.framework.Assert.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Integration test case for Applink related requests going throw the {@link RestServletFilter}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ApplinksRESTITCase extends BaseITCase {

	/**
	 * Test case for the {@code /rest/applinks/1.0/manifest} request
	 *
	 * @throws Exception
	 */
	@Test
	public void testGetManifest() throws Exception {
		XmlPage page = webClient.getPage("http://localhost:9000/rest/applinks/1.0/manifest");
		Element documentElement = page.getXmlDocument().getDocumentElement();
		assertThat(documentElement.getTagName(), is("manifest"));
		assertThat(documentElement.getChildNodes().getLength(), is(10));
		assertThat(getElementContent(documentElement, "id"), is("0a0611b4-2c0d-30ad-85d9-791f5fe3c9a6"));
		assertThat(getElementContent(documentElement, "name"), is("SonarQube"));
		assertThat(getElementContent(documentElement, "typeId"), is("sonarqube"));
		assertThat(getElementContent(documentElement, "version"), is("3.6.2"));
		assertThat(getElementContent(documentElement, "applinksVersion"), is("4.0.6"));
		assertThat(getElementContent(documentElement, "inboundAuthenticationTypes"), is("com.atlassian.applinks.api.auth.types" +
				".BasicAuthenticationProvider"));
		assertThat(getElementContent(documentElement, "outboundAuthenticationTypes"), is("com.atlassian.applinks.api.auth.types" +
				".BasicAuthenticationProvider"));
		assertThat(getElementContent(documentElement, "publicSignup"), is("true"));
		assertThat(getElementContent(documentElement, "url"), is("http://localhost:9000"));
		assertThat(getElementContent(documentElement, "iconUrl"), is("http://localhost:9000/static/applinks/images/icon16_sonarqube.png"));
	}

	/**
	 * Test case for the {@code /rest/applinks/1.0/permission} request
	 *
	 * @throws Exception
	 */
	@Test
	public void testGetPermissionAnonymously() throws Exception {
		XmlPage page = webClient.getPage("http://localhost:9000/rest/applinks/1.0/permission");
		Element documentElement = page.getXmlDocument().getDocumentElement();
		assertThat(getElementContent(documentElement, "code"), is("NO_PERMISSION"));
	}

	/**
	 * Test case for the {@code /rest/applinks/1.0/permission} request
	 *
	 * @throws Exception
	 */
	@Test
	public void testGetPermissionWithUser() throws Exception {
		login("user");
		XmlPage page = webClient.getPage("http://localhost:9000/rest/applinks/1.0/permission");
		Element documentElement = page.getXmlDocument().getDocumentElement();
		assertThat(getElementContent(documentElement, "code"), is("NO_PERMISSION"));
	}

	/**
	 * Test case for the {@code /rest/applinks/1.0/permission} request
	 *
	 * @throws Exception
	 */
	@Test
	public void testGetPermissionWithAdmin() throws Exception {
		login("admin");
		XmlPage page = webClient.getPage("http://localhost:9000/rest/applinks/1.0/permission");
		Element documentElement = page.getXmlDocument().getDocumentElement();
		assertThat(getElementContent(documentElement, "code"), is("ALLOWED"));
	}

	/**
	 * Test case for the {@code /rest/applinks/1.0/entities} request
	 *
	 * @throws Exception
	 */
	@Test
	public void testGetEntities() throws Exception {
		XmlPage page = webClient.getPage("http://localhost:9000/rest/applinks/1.0/entities");
		NodeList list = page.getXmlDocument().getElementsByTagName("entity");
		assertThat(list.getLength(), is(1));
		NamedNodeMap attributes = list.item(0).getAttributes();
		assertThat(attributes.getNamedItem("iconUrl").getTextContent(),
				is("http://localhost:9000/static/applinks/images/icon16_sonarqube.resource.png"));
		assertThat(attributes.getNamedItem("typeId").getTextContent(), is("sonarqube.resource"));
		assertThat(attributes.getNamedItem("name").getTextContent(), is("UT coverage with Maven and Cobertura running tests"));
		assertThat(attributes.getNamedItem("key").getTextContent(), is("org.codehaus.sonar:example-ut-maven-cobertura-runTests"));
	}

	/**
	 * Test case for the {@code /rest/applinks/1.0/entities} request
	 *
	 * @throws Exception
	 */
	@Test
	public void testGetEntitiesWithAdmin() throws Exception {
		login("admin");
		XmlPage page = webClient.getPage("http://localhost:9000/rest/applinks/1.0/entities");
		NodeList list = page.getXmlDocument().getElementsByTagName("entity");
		assertThat(list.getLength(), is(2));
		NamedNodeMap attributes = list.item(0).getAttributes();
		assertThat(attributes.getNamedItem("iconUrl").getTextContent(),
				is("http://localhost:9000/static/applinks/images/icon16_sonarqube.resource.png"));
		assertThat(attributes.getNamedItem("typeId").getTextContent(), is("sonarqube.resource"));
		assertThat(attributes.getNamedItem("name").getTextContent(), is("Simple Java Maven Project"));
		assertThat(attributes.getNamedItem("key").getTextContent(), is("org.codehaus.sonar:example-java-maven"));
		attributes = list.item(1).getAttributes();
		assertThat(attributes.getNamedItem("iconUrl").getTextContent(),
				is("http://localhost:9000/static/applinks/images/icon16_sonarqube.resource.png"));
		assertThat(attributes.getNamedItem("typeId").getTextContent(), is("sonarqube.resource"));
		assertThat(attributes.getNamedItem("name").getTextContent(), is("UT coverage with Maven and Cobertura running tests"));
		assertThat(attributes.getNamedItem("key").getTextContent(), is("org.codehaus.sonar:example-ut-maven-cobertura-runTests"));
	}

	/**
	 * Test case to verify that the {@code /rest/applinks/1.0/authenticationinfo} service is for admins only
	 *
	 * @throws Exception
	 */
	@Test
	public void testAuthenticationinfoNoAdmin() throws Exception {
		try {
			webClient.getPage("http://localhost:9000/rest/applinks/1.0/authenticationinfo/id/" + ApplicationIdUtilsTest
					.MARVELUTION_ISSUES_ID + "/url/" + ApplicationIdUtilsTest.HTTPS_MARVELUTION_ATLASSIAN_NET);
			fail("Rest action '/rest/applinks/1.0/authenticationinfo/id/[id]/url/[url]' should require admin access!");
		} catch (FailingHttpStatusCodeException e) {
			assertThat(e.getStatusCode(), is(401));
		}
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/authenticationinfo}
	 *
	 * @throws Exception
	 */
	@Test
	public void testAuthenticationinfoNoIdOrUrl() throws Exception {
		login("admin");
		Page page = webClient.getPage("http://localhost:9000/rest/applinks/1.0/authenticationinfo");
		assertThat(page.getWebResponse().getStatusCode(), is(200));
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/authenticationinfo}
	 *
	 * @throws Exception
	 */
	@Test
	public void testAuthenticationinfo() throws Exception {
		login("admin");
		Page page = webClient.getPage("http://localhost:9000/rest/applinks/1.0/authenticationinfo/id/" + ApplicationIdUtilsTest
				.MARVELUTION_ISSUES_ID + "/url/" + ApplicationIdUtilsTest.HTTPS_MARVELUTION_ATLASSIAN_NET);
		assertThat(page.getWebResponse().getStatusCode(), is(200));
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/authenticationinfo}
	 *
	 * @throws Exception
	 */
	@Test
	public void testAuthenticationinfoNotFound() throws Exception {
		login("admin");
		try {
			webClient.getPage("http://localhost:9000/rest/applinks/1.0/authenticationinfo/id/" + ApplicationIdUtilsTest
					.MARVELUTION_ISSUES_ID + "/url/http://fake.url");
		} catch (FailingHttpStatusCodeException e) {
			assertThat(e.getStatusCode(), is(404));
		}
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/applicationlink}
	 *
	 * @throws Exception
	 */
	@Test
	public void testApplicationlinkNoAdmin() throws Exception {
		try {
			webClient.getPage("http://localhost:9000/rest/applinks/1.0/applicationlink");
			fail("Rest action '/rest/applinks/1.0/applicationlink' should require admin access!");
		} catch (FailingHttpStatusCodeException e) {
			assertThat(e.getStatusCode(), is(401));
		}
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/applicationlink}
	 *
	 * @throws Exception
	 */
	@Test
	public void testApplicationlinkBadRequest() throws Exception {
		login("admin");
		try {
			webClient.getPage("http://localhost:9000/rest/applinks/1.0/applicationlink");
			fail("No Applink ID was given that is required!");
		} catch (FailingHttpStatusCodeException e) {
			assertThat(e.getStatusCode(), is(400));
		}
		try {
			webClient.getPage("http://localhost:9000/rest/applinks/1.0/applicationlink/" + ApplicationIdUtilsTest.MARVELUTION_ISSUES_ID);
			fail("GET is not supported by the /rest/applinks/1.0/applicationlink service!");
		} catch (FailingHttpStatusCodeException e) {
			assertThat(e.getStatusCode(), is(400));
		}
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/applicationlink}
	 *
	 * @throws Exception
	 */
	@Test
	@Ignore("Not yet implemented, but should be soon")
	public void testAddApplicationlink() throws Exception {
		login("admin");
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/applicationlink}
	 *
	 * @throws Exception
	 */
	@Test
	@Ignore("Not yet implemented, but should be soon")
	public void testDeleteApplicationlink() throws Exception {
		login("admin");
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/entitylink}
	 *
	 * @throws Exception
	 */
	@Test
	public void testEntitylinkNoAdmin() throws Exception {
		try {
			webClient.getPage("http://localhost:9000/rest/applinks/1.0/entitylink");
			fail("Rest action '/rest/applinks/1.0/entitylink' should require admin access!");
		} catch (FailingHttpStatusCodeException e) {
			assertThat(e.getStatusCode(), is(401));
		}
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/entitylink}
	 *
	 * @throws Exception
	 */
	@Test
	public void testEntitylinkBadRequest() throws Exception {
		login("admin");
		try {
			webClient.getPage("http://localhost:9000/rest/applinks/1.0/entitylink");
			fail("No Applink ID was given that is required!");
		} catch (FailingHttpStatusCodeException e) {
			assertThat(e.getStatusCode(), is(400));
		}
		try {
			webClient.getPage("http://localhost:9000/rest/applinks/1.0/entitylink/sonarqube.resource/org.codehaus" +
					".sonar:example-java-maven");
			fail("GET is not supported by the /rest/applinks/1.0/entitylink service!");
		} catch (FailingHttpStatusCodeException e) {
			assertThat(e.getStatusCode(), is(400));
		}
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/entitylink}
	 *
	 * @throws Exception
	 */
	@Test
	@Ignore("Not yet implemented, but should be soon")
	public void testAddEntitylink() throws Exception {
		login("admin");
	}

	/**
	 * Test case for {@code /rest/applinks/1.0/entitylink}
	 *
	 * @throws Exception
	 */
	@Test
	@Ignore("Not yet implemented, but should be soon")
	public void testDeleteEntitylink() throws Exception {
		login("admin");
	}

}
