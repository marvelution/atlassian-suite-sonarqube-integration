/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube;

import com.google.common.collect.ImmutableList;
import org.sonar.api.CoreProperties;
import org.sonar.api.PropertyType;
import org.sonar.api.ServerExtension;
import org.sonar.api.config.PropertyDefinition;
import org.sonar.api.config.Settings;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ApplinksSettings implements ServerExtension {

	public static final String PROPERTY_CATEGORY = "Atlassian Applinks";
	public static final String APPLINKS_APP_NAME = "applinks.app.name";
	public static final String APPLINKS_APP_NAME_DEFAULT_VALUE = "SonarQube";
	public static final String APPLINKS_URL = CoreProperties.SERVER_BASE_URL;
	public static final String APPLINKS_URL_DEFAULT_VALUE = CoreProperties.SERVER_BASE_URL_DEFAULT_VALUE;

	private final Settings settings;

	public ApplinksSettings(Settings settings) {
		this.settings = settings;
	}

	private String getProperty(String key) {
		return settings.getString(key);
	}

	public static Iterable<PropertyDefinition> getPropertyDefinitions() {
		return ImmutableList.of(
				PropertyDefinition.builder(APPLINKS_APP_NAME).defaultValue(APPLINKS_APP_NAME_DEFAULT_VALUE).category(PROPERTY_CATEGORY)
						.name("Application Name").description("The name used to identify this SonarQube instance among others on linked " +
						"applications like JIRA and Bamboo").type(PropertyType.STRING).index(1).build()
		);
	}

}
